package testControle;

import static org.junit.Assert.*;

import model.Pessoa;

import org.junit.Before;
import org.junit.Test;

import controle.ControlePessoa;

public class controleTest {
	
	private Pessoa novaPessoa;
	private ControlePessoa novaPessoaControle;
	
	@Before
	public void setUp() throws Exception{
		novaPessoa = new Pessoa();
		novaPessoaControle = new ControlePessoa();
	}

	@Test
	public void testAdicionar() {
		assertEquals("Pessoa adicionada com Sucesso!", novaPessoaControle.adicionar(novaPessoa));
	}
	
	@Test
	public void testRemover() {
		assertEquals("Pessoa removida com Sucesso!", novaPessoaControle.remover(novaPessoa));
	}
	
	@Test
	public void testPesquisar() {
		String novoNome = "Omar";
		novaPessoa.setNome(novoNome);
		novaPessoaControle.adicionar(novaPessoa);
		
		assertEquals(novaPessoa, novaPessoaControle.pesquisar(novoNome));
	}

}
