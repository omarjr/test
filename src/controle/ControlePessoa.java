package controle;

import java.util.ArrayList;

import model.Pessoa;

public class ControlePessoa {
//atributos

    private ArrayList<Pessoa> listaPessoas;

//construtor

    public ControlePessoa() {
        listaPessoas = new ArrayList<Pessoa>();
    }

// métodos
   
    public String adicionar(Pessoa umaPessoa) {
        String mensagem = "Pessoa adicionada com Sucesso!";
        listaPessoas.add(umaPessoa);
    	return mensagem;
        }
	

    public String remover(Pessoa umaPessoa) {
        String mensagem = "Pessoa removida com Sucesso!";
        listaPessoas.remove(umaPessoa);
    	return mensagem;
        }
	
    
    public Pessoa pesquisar(String nome) {
        for (Pessoa pessoa: listaPessoas) {
            if (pessoa.getNome().equalsIgnoreCase(nome)) return pessoa;
        }
        return null;
    }

}
