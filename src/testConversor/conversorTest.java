package testConversor;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import conversor.ConversorTemperatura;

public class conversorTest {
	
	private ConversorTemperatura meuConversor;

	@Before
	public void setUp() throws Exception{
		meuConversor = new ConversorTemperatura();
	}
	
	@Test
	public void testCelsiusParaFahrenheitComValorCelsiusDouble() {
		assertEquals(104.0, meuConversor.celsiusParaFahrenheit(40.0), 0.01);
	}
	
	@Test
	public void testfFahrenheitParaCelsiusComValorFahrenheitDouble() {
		assertEquals(40.0, meuConversor.fahrenheitParaCelsius(104.0), 0.01);
	}
	
	@Test
	public void testfCelsiusParaKelvinComValorCelsiusDouble() {
		assertEquals(273, meuConversor.celsiusParaKelvin(0.0), 0.01);
	}

	@Test
	public void testfKelvinParaCelsiusComValorKelvinDouble() {
		assertEquals(0.0, meuConversor.kelvinParaCelsius(273.0), 0.01);
	}
	
	@Test
	public void testfFahrenheitParaKelvinComValorFahrenheitDouble() {
		assertEquals(373.0, meuConversor.fahrenheitParaKelvin(212.0), 0.01);
	}
	
	@Test
	public void testfkelvinParaFahrenheitComValorKelvinDouble() {
		assertEquals(212.0, meuConversor.kelvinParaFahrenheit(373.0), 0.01);
	}
}
