package testPessoa;

import static org.junit.Assert.*;

import model.Pessoa;

import org.junit.Before;
import org.junit.Test;

public class PessoaTest {
	
	private Pessoa umaPessoaTest;

	@Before
	public void setUp() throws Exception{
		umaPessoaTest = new Pessoa();
	}
	
	@Test
	public void testNome() {
		umaPessoaTest.setNome("Omar");
		assertEquals(umaPessoaTest.getNome(), "Omar");
	}
	
	@Test
	public void testIdade() {
		umaPessoaTest.setIdade("10");
		assertEquals(umaPessoaTest.getIdade(), "10");
	}
	
	@Test
	public void testTelefone() {
		umaPessoaTest.setTelefone("555-5555");
		assertEquals(umaPessoaTest.getTelefone(), "555-5555");
	}
	
	@Test
	public void testSexo() {
		umaPessoaTest.setSexo("Masculino");
		assertEquals(umaPessoaTest.getSexo(), "Masculino");
	}
	
	@Test
	public void testEmail() {		
		umaPessoaTest.setEmail("omar@omar.com");		
		assertEquals(umaPessoaTest.getEmail(), "omar@omar.com");
	}
	
	@Test
	public void testHangout() {
		umaPessoaTest.setHangout("222-2222");
		assertEquals(umaPessoaTest.getHangout(), "222-2222");
	}
	
	@Test
	public void testEndereco() {
		umaPessoaTest.setEndereco("QNN 00 Casa 0");
		assertEquals(umaPessoaTest.getEndereco(), "QNN 00 Casa 0");
	}
	
	@Test
	public void testRg() {
		umaPessoaTest.setRg("12345678");
		assertEquals(umaPessoaTest.getRg(), "12345678");
	}
	
	@Test
	public void testCpf() {
		umaPessoaTest.setCpf("1122334455");
		assertEquals(umaPessoaTest.getCpf(), "1122334455");
	}
	
	

}
